
require('./bootstrap');

window.Vue = require('vue');
window.axios = require('axios');
import Vue from 'vue'
import * as VueGoogleMaps from 'vue2-google-maps'

Vue.use(VueGoogleMaps, {
    load: {
        key: 'AIzaSyBZq0vMU3ub8QSmf6xkv5YW_28LRAuimhM',
        libraries: 'places', // This is required if you use the Autocomplete plugin
        // OR: libraries: 'places,drawing'
        // OR: libraries: 'places,drawing,visualization'
        // (as you require)

        //// If you want to set the version, you can do so:
        // v: '3.26',
    },
})

Vue.component('Location', require('./components/Location.vue').default);
// Vue.component('User', require('./components/User.vue').default);
// Vue.component('Create', require('./components/Create.vue').default);
// Vue.component('Edit', require('./components/Edit.vue').default);


const app = new Vue({
    el: '#app',
});
