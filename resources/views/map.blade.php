<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <link rel="stylesheet" href="{{url('css/app.css')}}">
    <!-- Styles -->

</head>
<body>
<div class="flex-center position-ref full-height">
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" href="#">Bootstrap ,PHP laravel,Vue Js</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                {{--<li class="nav-item active">--}}
                    {{--<a class="nav-link" href="#">PHP laravel <span class="sr-only">(current)</span></a>--}}
                {{--</li>--}}
                {{--<li class="nav-item active">--}}
                    {{--<a class="nav-link" href="#">Vue Js <span class="sr-only">(current)</span></a>--}}
                {{--</li>--}}
            </ul>

        </div>
    </nav>
    <div class="container">
        <div id="app">
            <Location data-lat="{{@$lat}}" data-lng="{{ @$lng }}"></Location>
        </div>
    </div>
</div>
</body>
<script>
</script>
<script src="{{url('js/app.js')}}" charset="utf-8"></script>

</html>


